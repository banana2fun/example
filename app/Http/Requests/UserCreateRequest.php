<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'string|min:3',
            'email' => 'email|unique:users,email',
            'role_id' => 'exists:roles,id',
            'password' => 'string|min:3',
        ];
    }
}
