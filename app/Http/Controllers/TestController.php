<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkParam')->only('hello');
    }

    public function hello()
    {
        return 2;
    }

    public function goodbuy()
    {
        return 3;
    }
}
