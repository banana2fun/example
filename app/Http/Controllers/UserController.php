<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users = User::select()
            ->where('role_id', '=', 1)
            ->with(['posts'])
            ->get();

        $result = [];

        foreach ($users as $user) {
            $posts = [];
            foreach ($user->posts as $post) {
                $posts[] = $post->name;
            }
            $result[] = [
                'name' => $user->name,
                'role' => $user->role->name,
                'posts' => $posts,
            ];
        }

        return $result;
    }


    public function create()
    {
        $roles = Role::all();

        return view('users.create', compact('roles'));
    }


    public function store(UserCreateRequest $request)
    {
        $data = $request->validated();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'role_id' => $data['role_id'],
        ]);
    }


    public function show($id)
    {
        $user = User::find($id);

        return [
            'name' => $user->name,
            'role' => $user->role->name,
            'posts' => $user->posts,
        ];
    }


    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);

        return view('users.update', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $user = User::find($id);
        $user->name = $data['name'];
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::destroy($id);
    }
}
