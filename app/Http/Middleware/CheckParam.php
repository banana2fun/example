<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class CheckParam extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        if (!$request->get('name')) {
            return redirect(RouteServiceProvider::HOME);
        }
        return $next($request);
    }
}
