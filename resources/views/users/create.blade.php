<form>
    <input id="name">
    <label for="name">Name</label>
    <input id="email">
    <label for="email">email</label>
    <input id="password">
    <label for="password">password</label>
    <select name="role" id="role">
        @foreach($roles as $role)
            <option value="{{$role->id}}">{{$role->name}}</option>
        @endforeach
    </select>
</form>

