<form>
    <input id="name" value="{{$user->name}}">
    <label for="name">Name</label>
    <input id="email" value="{{$user->email}}">
    <label for="email">email</label>
    <input id="password" value="{{$user->password}}">
    <label for="password">password</label>
    <select name="role" id="role">
        @foreach($roles as $role)
            @if ($user->role_id === $role->id)
                <option value="{{$role->id}}" selected>{{$role->name}}</option>
            @else
                <option value="{{$role->id}}">{{$role->name}}</option>
            @endif
        @endforeach
    </select>
</form>

